"""
gcn 的dgl实现

"""
#%%
import dgl
import torch
from torch import nn
from torch.nn import functional as F
import numpy as np
from dataset import get_edge_list

#%%

def build_graph():
    edges, labels = get_edge_list()
    src, tgt = tuple(zip(*edges))
    labels = torch.tensor(labels, dtype=torch.int64)

    g = dgl.DGLGraph()
    g.add_nodes(len(labels))
    g.add_edges(src, tgt)
    return g, labels


g, labels = build_graph()
print(g.number_of_nodes(), g.number_of_edges())

N = g.number_of_nodes()
g.ndata["feat"] = torch.eye(N)

print(g.nodes[[2, 10]].data["feat"])
print(g.edges)


def gcn_message(edges):

    return {"msg": edges.src["h"]}


def gcn_reduce(nodes):

    return {"h": torch.sum(nodes.mailbox["msg"], dim=1)}


class GCNLayer(nn.Module):
    def __init__(self, in_feats, out_feats):
        super().__init__()
        self.linear = nn.Linear(in_feats, out_feats)

    def forward(self, g: dgl.DGLGraph, inputs):

        g.ndata['h'] = inputs
        g.send(g.edges(), gcn_message)
        g.recv(g.nodes(), gcn_reduce)
        h = g.ndata.pop('h')
        return self.linear(h)


class GCN(nn.Module):

    def __init__(self, dim_in, dim_hidden, dim_out):
        super().__init__()
        self.layer1 = GCNLayer(dim_in, dim_in)
        self.layer2 = GCNLayer(dim_in, dim_in//2)
        self.layer3 = GCNLayer(dim_in//2, dim_out)

    def forward(self, g, inputs):
        h = F.relu(self.layer1(g, inputs))
        h = F.relu(self.layer2(g, h))
        h = self.layer3(g, h)
        return h


gcn = GCN(N, N, 2)

inputs = torch.eye(N)

label_mask = torch.tensor([0, -1])
label_nodes = torch.tensor([0, 1])

optimizer = torch.optim.Adam(gcn.parameters(), lr=0.01)

for epoch in range(100):

    logit = gcn(g, inputs)
    log_p = F.log_softmax(logit, dim=-1)
    loss = F.nll_loss(log_p[label_mask], label_nodes)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    accu = (torch.argmax(log_p, dim=1) == labels).float().mean().item()

    print(f"Epoch {epoch}:  Loss: {loss.item()}, accu: {accu}")
