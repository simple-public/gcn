import numpy as np
import torch

from torch import nn
from torch.nn import functional as F
from torch import optim
from dataset import get_data


class GraphCN(nn.Module):

    def __init__(self, A, dim_in, dim_out):
        super().__init__()

        A = normalize(A)
        self.A = torch.FloatTensor(A)
        self.layer1 = nn.Linear(dim_in, dim_in, bias=False)
        self.layer2 = nn.Linear(dim_in, dim_in//2, bias=False)
        self.layer3 = nn.Linear(dim_in//2, dim_out, bias=False)

        nn.init.xavier_normal_(self.layer1.weight)
        nn.init.xavier_normal_(self.layer2.weight)
        nn.init.xavier_normal_(self.layer3.weight)

    def forward(self, X):

        A = self.A
        X = F.relu(self.layer1(torch.mm(A, X)))
        X = F.relu(self.layer2(torch.mm(A, X)))
        Z = self.layer3(torch.mm(A, X))
        return Z


def normalize(A):

    d = np.sum(A, axis=1)
    D_sqrt = np.diag(np.power(d, -0.5))
    A_norm = D_sqrt @ A @ D_sqrt
    return A_norm


A, labels = get_data()
N, _ = A.shape
#labels = labels[:, 1]

#print(A, labels)

X_dim = N

gcn = GraphCN(A, X_dim, 2)

lr = 0.005
params = list(filter(lambda x: x.requires_grad, gcn.parameters()))
#optimizer = optim.SGD(params, lr=lr, momentum=0.9)
optimizer = torch.optim.Adam(params, lr=0.01)

X = torch.eye(N, X_dim)
y = torch.LongTensor(labels).view(-1, 1).contiguous()

mask = torch.zeros(N, dtype=torch.uint8)
mask[[0, -1]] = 1

for epoch in range(100):

    optimizer.zero_grad()
    log_p = F.log_softmax(gcn(X), dim=-1)
    y_pred = torch.gather(-log_p, 1, y).view(-1)

    loss = torch.masked_select(y_pred, mask).sum()
    #loss = F.nll_loss(log_p[mask], y[mask].view(-1))
    loss.backward()
    optimizer.step()

    if (epoch + 1) % 20:
        accu = (torch.argmax(log_p, dim=1) == y.view(-1)).float().mean().item()

        print(f"epoch: {epoch}, loss: {loss.item()}, accu: {accu}")


