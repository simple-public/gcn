
import networkx as nx
import numpy as np
from scipy.sparse import coo_matrix
import os.path
import os


def base_path():
    return os.path.abspath(os.path.dirname(__file__))


DEFAULT_PATH = os.path.join(base_path(), "data", "zkcc-77")


def get_club_data():
    G = nx.karate_club_graph()
    A = nx.adjacency_matrix(G).toarray()
    return A


def read_edges(path):

    with open(path) as f:

        data = []
        for line in f:
            start, end = line.strip("\n").split()
            start, end = int(start)-1, int(end)-1
            data.append((start, end))

        return data


def read_label(path):
    with open(path) as f:
        data = []
        for line in f:
            node, label = line.rstrip("\n").split()
            node, label = int(node), int(label)
            data.append((node-1, label-1))

        _, labels = list(zip(*data))

        return np.array(labels)


def build_adjacent_matrix(edges):
    start, end = list(zip(*edges))
    # start, end = start + end, end + start
    ones = [1] * len(start)
    data = coo_matrix((ones, (start, end)))
    return data.toarray()


def get_data(path=DEFAULT_PATH):
    import os.path
    edge_path = os.path.join(path, "karate_edges_77.txt")
    label_path = os.path.join(path, "karate_groups.txt")

    edges = read_edges(edge_path)
    adjacent_matrix = build_adjacent_matrix(edges)
    labels = read_label(label_path)
    return adjacent_matrix, labels


def get_edge_list(path=DEFAULT_PATH):
    import os.path
    edge_path = os.path.join(path, "karate_edges_77.txt")
    label_path = os.path.join(path, "karate_groups.txt")

    edges = read_edges(edge_path)
    labels = read_label(label_path)
    return edges, labels


if __name__ == "__main__":
    data, labels = get_data()
    print(data.shape)
    print(type(data))